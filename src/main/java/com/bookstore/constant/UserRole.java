package com.bookstore.constant;

import java.util.Arrays;

public enum UserRole {

    DELETED(0), USER(1), ADMIN(2);

    UserRole(int id) {
        this.id = id;
    }

    private int id;

    String successpage;

    private int priority;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setSuccesspage(String successpage) {
        this.successpage = successpage;
    }

    public String getSuccesspage() {
        choospage(1);
        return successpage;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public void choospage(int role) {
        if (role == 0) {
            this.setSuccesspage("/common/welcome");
            //       page = "/web/welcome";
        } else if (role == 1) {
            this.setSuccesspage("/user/");
            //  page = "/web/user";
        } else if (role == 2) {
            this.setSuccesspage("/store/");
            //  page = "/web/admin";
        }
    }

    public static UserRole from(int role) {
        System.out.println("From Role : " + role);
        return Arrays.stream(values())
                .filter(f -> f.id == role)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown user role " + role));
    }
}
