package com.bookstore.service;

import com.bookstore.domain.Books;
import com.bookstore.domain.DataTable;
import com.bookstore.domain.PaginationResult;
import com.bookstore.domain.Store;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface BookService {
    PaginationResult<Books> getBookList(int limit, int offset);

    DataTable getBookDataTable(int draw,int start,int lenth,String search,int SortColumn,String sortDirection);

    List<Books> getBookListById(long id);

    Books addBook(Books books);

    void deleteBook(long id);

    void updateBook(Books books);
}
