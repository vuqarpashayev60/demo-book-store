package com.bookstore.service;

import com.bookstore.domain.DataTable;
import com.bookstore.domain.PaginationResult;
import com.bookstore.domain.RegistrationForm;
import com.bookstore.domain.User;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.Optional;

public interface UserService {
    PaginationResult<User> getUserList(int limit, int offset);

    DataTable getUserDataTable(int draw, int start, int length, String search, int SortColumn, String sortDirection);

    List<User> getUserListById(long id);

    Optional<User> getUserListByUsername(String username);

    User addUser(RegistrationForm registrationForm, BindingResult errors);

    void updateUser(RegistrationForm registrationForm, BindingResult errors);

    void deleteUser(long id);
}
