package com.bookstore.service;

import com.bookstore.domain.DataTable;
import com.bookstore.domain.PaginationResult;
import com.bookstore.domain.Store;
import com.bookstore.domain.User;

import java.util.List;
import java.util.Optional;

public interface StoreService {

    PaginationResult<Store> getStoreList(int limit, int offset);

    DataTable getStoreDataTable(int draw, int start, int lenth, String search, int SortColumn, String sortDirection);

    List<Store> getStoreName();

    List<Store> getStoreListById(long id);

    Store addStore(Store store);

    void updateStore(Store store);

    void deleteStore(long id);
}
