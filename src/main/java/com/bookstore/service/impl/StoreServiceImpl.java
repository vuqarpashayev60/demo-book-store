package com.bookstore.service.impl;

import com.bookstore.domain.DataTable;
import com.bookstore.domain.PaginationResult;
import com.bookstore.domain.Store;
import com.bookstore.repository.StoreRepo;
import com.bookstore.service.StoreService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class StoreServiceImpl implements StoreService {

    private static final Logger logger = Logger.getLogger(StoreService.class);

    @Autowired
    StoreRepo storeRepo;




    @Override
    public PaginationResult<Store> getStoreList(int limit, int offset) {
        PaginationResult<Store> paginationResult = null;
        int total = 0;
        try {
            List<Store> storeList = storeRepo.getStoreList(limit, offset);
            try {
                total = storeRepo.countStore();
            } catch (Exception e) {
                logger.error("StoreService method getStoreList count Store : Error Message : " + e.getMessage());
            }
            if (!storeList.isEmpty() || total != 0) {
                paginationResult = new PaginationResult<>();
                paginationResult.setList(storeList);
                paginationResult.setLimit(limit);
                paginationResult.setOffset(offset);
                paginationResult.setTotal(total);
                paginationResult.setFiltered(storeList.size());
                logger.info("StoreService method getStoreList :  Successful List : " + storeList);
            } else {
                logger.error("StoreService getStore List : List Is Empty");
                throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Store List Is Empty");
            }
        } catch (Exception e) {
            logger.error("StoreService method getStoreList : Error Message : " + e.getMessage());
        }
        return paginationResult;
    }

    @Override
    public DataTable getStoreDataTable(int draw, int start, int lenth, String search, int sortColumn, String sortDirection) {
        DataTable dataTable = new DataTable();
        try {
            dataTable.setDraw(draw);
            dataTable.setStart(start);
            dataTable.setLength(lenth);
            dataTable.setSearch(search);
            dataTable.setSortColumn(++sortColumn);
            dataTable.setSortDirection(sortDirection);

            dataTable.setRecordsTotal(storeRepo.countStore());


            if (dataTable.getSearch() != null && !dataTable.getSearch().isEmpty()) {
                dataTable.setRecordsFiltered(storeRepo.countSearchStoreList(dataTable.getSearch()));
            } else {
                dataTable.setRecordsFiltered(dataTable.getRecordsTotal());
            }

            List<Store> storeList = storeRepo.searchStoreList(dataTable.getSearch(), dataTable.getStart(), dataTable.getLength());
            dataTable.setData(new Object[storeList.size()][4]);
            for (int i = 0; i < storeList.size(); i++) {
                dataTable.getData()[i][0] = i + 1;
                dataTable.getData()[i][1] = storeList.get(i).getName();
                dataTable.getData()[i][2] = storeList.get(i).getAddress();
                dataTable.getData()[i][3] =
                        "<a href='#' onclick='show_store_details(" + storeList.get(i).getId() + ")'>View</a>" +
                                " <a href='#' onclick='show_store_update(" + storeList.get(i).getId() + ")'>Edit</a>" +
                                " <a href='#' onclick='show_store_delete(" + storeList.get(i).getId() + ")'>Delete</a>";

                logger.info("StoreService method getDataTable : Successful Datatable list : " + dataTable);
            }

        } catch (Exception e) {
            logger.error("StoreService method getDataTable : Error Message :  " + e.getMessage());
        }
        return dataTable;
    }

    @Override
    public List<Store> getStoreName() {
        List<Store> storeList = null;
        try {
            storeList = storeRepo.getStoreName();
            logger.info("StoreService method getStoreName : Successful");
        } catch (Exception e) {
            logger.error("StoreService method getStoreName : Error Message :  " + e.getMessage());
        }
        return storeList;
    }

    @Override
    public List<Store> getStoreListById(long storeId) {
        List<Store> storeList = null;
        try {
            Optional<Store> optionalStore = storeRepo.getStoreListById(storeId);
            if (optionalStore.isPresent()) {
                storeList = optionalStore.stream().toList();
                logger.info("StoreService method getStoreListById=" + storeId + "Is Successful List : " + storeList);
            } else {
                logger.error("StoreService method getStoreListById=" + storeId + "Error Message  : " + HttpStatus.NOT_FOUND);
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Store with id " + storeId + " not found");
            }
        } catch (Exception e) {
            logger.error("StoreService method getStoreListById=" + storeId + "Error Message : " + e.getMessage());
        }
        return storeList;
    }

    @Transactional
    @Override
    public Store addStore(Store store) {
        Store store1 = new Store();
        try {
            store1 = storeRepo.addStore(store);
            logger.info("StoreService method addStore : Successful Store = " + store1);
        } catch (Exception e) {
            logger.error("StoreService method addStore Error Message : " + e.getMessage());
        }
        return store1;
    }

    @Override
    public void updateStore(Store store) {
        try {
            storeRepo.updateStore(store);
            logger.info("StoreService method updateStore : Successful Store =  " + store);
        } catch (Exception e) {
            logger.error("StoreService method updateStore Error Message : " + e.getMessage());
        }
    }

    @Override
    public void deleteStore(long storeId) {
        try {
            storeRepo.deleteStore(storeId);
            logger.info("StoreService method deleteStore : Successful Store Id = " + storeId);
        } catch (Exception e) {
            logger.error("StoreService method deleteStore Error Message : " + e.getMessage());
        }
    }
}
