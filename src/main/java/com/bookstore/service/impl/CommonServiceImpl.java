package com.bookstore.service.impl;

import com.bookstore.repository.CommonRepo;
import com.bookstore.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommonServiceImpl implements CommonService {
    @Autowired
    CommonRepo commonRepo;
    @Override
    public boolean checkUserName(String userName) {
        return commonRepo.checkUserName(userName);
    }
}
