package com.bookstore.service.impl;

import com.bookstore.domain.Books;
import com.bookstore.domain.DataTable;
import com.bookstore.domain.PaginationResult;
import com.bookstore.repository.BookRepo;
import com.bookstore.service.BookService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    private static final Logger logger = Logger.getLogger(BookService.class);

    @Autowired
    private BookRepo bookRepo;

    @Override
    public PaginationResult<Books> getBookList(int limit, int offset) {
        PaginationResult<Books> paginationResult = null;
        try {
            List<Books> booksList = bookRepo.getBooksList(limit, offset);
            int total = 0;
            try {
                total = bookRepo.countAllBook();
            } catch (Exception e) {
                logger.error("BookService method getStoreList count Book : Error Message : " + e.getMessage());
            }
            if (!booksList.isEmpty() || total != 0) {
                paginationResult = new PaginationResult<>();
                paginationResult.setList(booksList);
                paginationResult.setLimit(limit);
                paginationResult.setOffset(offset);
                paginationResult.setTotal(total);
                paginationResult.setFiltered(booksList.size());
                logger.info("BookService method getBookList :  Successful List : " + booksList);
            } else {
                logger.error("BookService getBook List : List Is Empty");
                throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Book List Is Empty");
            }

        } catch (Exception e) {
            logger.error("BookService method getBookList : Error Message : " + e.getMessage());
        }
        return paginationResult;
    }

    @Override
    public DataTable getBookDataTable(int draw, int start, int length, String search, int sortColumn, String sortDirection) {

        DataTable dataTable = new DataTable();
        try {
            dataTable.setDraw(draw);
            dataTable.setStart(start);
            dataTable.setLength(length);
            dataTable.setSearch(search);
            dataTable.setSortColumn(++sortColumn);
            dataTable.setSortDirection(sortDirection);

            dataTable.setRecordsTotal(bookRepo.countBook());


            if (dataTable.getSearch() != null && !dataTable.getSearch().isEmpty()) {
                dataTable.setRecordsFiltered(bookRepo.countSearchBookList(dataTable.getSearch()));
            } else {
                dataTable.setRecordsFiltered(dataTable.getRecordsTotal());
            }

            List<Books> booksList = bookRepo.searchBookList(dataTable.getSearch(), dataTable.getStart(), dataTable.getLength());
            dataTable.setData(new Object[booksList.size()][9]);
            for (int i = 0; i < booksList.size(); i++) {
                dataTable.getData()[i][0] = i + 1;
                dataTable.getData()[i][1] = booksList.get(i).getName();
                dataTable.getData()[i][2] = booksList.get(i).getAuthor();
                dataTable.getData()[i][3] = booksList.get(i).getPublisher();
                dataTable.getData()[i][4] = booksList.get(i).getYear();
                dataTable.getData()[i][5] = booksList.get(i).getPage();
                dataTable.getData()[i][6] = booksList.get(i).getPrice();
                dataTable.getData()[i][7] = booksList.get(i).getStore().getName();
                dataTable.getData()[i][8] =
                        "<a href='#' onclick='show_book_details(" + booksList.get(i).getId() + ")'>View</a>" +
                                " <a href='#' onclick='show_book_update(" + booksList.get(i).getId() + ")'>Edit</a>" +
                                " <a href='#' onclick='show_book_delete(" + booksList.get(i).getId() + ")'>Delete</a>";

                logger.info("BookService method getDataTable : Successful Datatable list : " + dataTable);
            }

        } catch (Exception e) {
            logger.error("BookService method getDataTable : Error Message :  " + e.getMessage());
        }
        return dataTable;
    }

    @Override
    public List<Books> getBookListById(long bookId) {
        List<Books> bookList = null;
        try {
            Optional<Books> optionalBooks = bookRepo.getBookListById(bookId);
            if (optionalBooks.isPresent()) {
                bookList = optionalBooks.stream().toList();
                logger.info("BookService method getBookListById=" + bookId + "Is Successful List : " + bookList);
            } else {
                System.out.println("Not present book");
                logger.error("BookService method getBookListById=" + bookId + "Error Message  : " + HttpStatus.NOT_FOUND);
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Book with id " + bookId + " not found");
            }
        } catch (Exception e) {
            logger.error("BookService method getBookListById=" + bookId + "Error Message : " + e.getMessage());
        }
        return bookList;
    }

    @Transactional
    @Override
    public Books addBook(Books books) {
        Books books1 = new Books();
        try {
            books1 = bookRepo.addBook(books);
            logger.info("BookService method addBook : Successful Book = " + books1);
        } catch (Exception e) {
            logger.error("BookService method addBook Error Message : " + e.getMessage());
        }
        return books1;
    }

    @Override
    public void updateBook(Books books) {
        try {
            bookRepo.updateBook(books);
            logger.info("BookService method updateBook : Successful Book =  " + books);
        } catch (Exception e) {
            logger.error("BookService method updateBook Error Message : " + e.getMessage());
        }
    }

    @Override
    public void deleteBook(long bookId) {
        try {
            bookRepo.deleteBook(bookId);
            logger.info("BookService method deleteBook : Successful Book Id = " + bookId);
        } catch (Exception e) {
            logger.error("BookService method deleteBook Error Message : " + e.getMessage());
        }
    }
}
