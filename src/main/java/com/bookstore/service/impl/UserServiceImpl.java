package com.bookstore.service.impl;

import com.bookstore.controller.api.UserController;
import com.bookstore.domain.*;
import com.bookstore.repository.UserRepo;
import com.bookstore.service.UserService;
import com.bookstore.util.RegistrationFormUtil;
import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = Logger.getLogger(UserService.class);

    @Autowired
    UserRepo userRepo;

    @Autowired
    MessageSource messageSource;

    List<String> errorList;

    @Override
    public PaginationResult<User> getUserList(int limit, int offset) {
        PaginationResult<User> paginationResult = null;
        List<User> userList = userRepo.getUserList(limit, offset);
        int total = userRepo.countUser();
        paginationResult = new PaginationResult<>();
        paginationResult.setList(userList);
        paginationResult.setLimit(limit);
        paginationResult.setOffset(offset);
        paginationResult.setTotal(total);
        paginationResult.setFiltered(userList.size());
        return paginationResult;
    }

    @Override
    public DataTable getUserDataTable(int draw, int start, int length, String search, int SortColumn, String sortDirection) {
        DataTable dataTable = new DataTable();
        try {
            dataTable.setDraw(draw);
            dataTable.setStart(start);
            dataTable.setLength(length);
            dataTable.setSearch(search);
            dataTable.setSortColumn(++SortColumn);
            dataTable.setSortDirection(sortDirection);

            dataTable.setRecordsTotal(userRepo.countUser());


            if (dataTable.getSearch() != null && !dataTable.getSearch().isEmpty()) {
                dataTable.setRecordsFiltered(userRepo.countSearchUserList(dataTable.getSearch()));
            } else {
                dataTable.setRecordsFiltered(dataTable.getRecordsTotal());
            }

            List<User> userList = userRepo.searchUserList(dataTable.getSearch(), dataTable.getStart(), dataTable.getLength());
            dataTable.setData(new Object[userList.size()][7]);
            for (int i = 0; i < userList.size(); i++) {
                dataTable.getData()[i][0] = i + 1;
                dataTable.getData()[i][1] = userList.get(i).getName();
                dataTable.getData()[i][2] = userList.get(i).getSurname();
                dataTable.getData()[i][3] = userList.get(i).getUsername();
                dataTable.getData()[i][4] = userList.get(i).getEmail();
                dataTable.getData()[i][5] = userList.get(i).getUserRole();
                dataTable.getData()[i][6] =
                        "<a href='#' onclick='show_user_details(" + userList.get(i).getId() + ")'>View</a>" +
                                " <a href='#' onclick='show_user_update(" + userList.get(i).getId() + ")'>Edit</a>" +
                                " <a href='#' onclick='show_user_delete(" + userList.get(i).getId() + ")'>Delete</a>";

                logger.info("UserService method getDataTable : Successful Datatable list : " + dataTable);
            }

        } catch (Exception e) {
            logger.error("UserService method getDataTable : Error Message :  " + e.getMessage());
        }
        return dataTable;
    }

    @Override
    public List<User> getUserListById(long userId) {
        List<User> userList = null;
        try {
            Optional<User> optionalUser = userRepo.getUserListById(userId);
            if (optionalUser.isPresent()) {
                userList = optionalUser.stream().toList();
            } else {
                logger.info("UserService method getUserListById=" + userId + "Is Successful List : " + userList);
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id " + userId + " not found");
            }
        } catch (Exception e) {
            logger.error("UserService method getUserListById=" + userId + "Error Message : " + e.getMessage());
        }
        return userList;
    }

    @Override
    public Optional<User> getUserListByUsername(String username) {
        return userRepo.getUserListByUsername(username);
    }

    @Transactional
    @Override
    public User addUser(RegistrationForm registrationForm, BindingResult errors) {
        User user = new User();
        if (errors.hasErrors()) {
            //Spring Error Handler
            errorList = new ArrayList<>();
            errors.getAllErrors().forEach(objectError -> {
                String msg = messageSource.getMessage(Objects.requireNonNull(objectError.getCode()), null, Locale.getDefault());
                errorList.add(msg);
            });
            logger.error("Field Error" + errorList.toString());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorList.toString());
        } else {
            try {
                user = RegistrationFormUtil.convert(registrationForm);
                String password = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
                user.setPassword(password);
                userRepo.addUser(user);
                logger.info("UserService method addUser Successful");
            } catch (Exception e) {
                logger.error("UserService method addUser Error Message : " + e.getMessage());
            }
        }
        return user;
    }

    @Override
    public void updateUser(RegistrationForm registrationForm, BindingResult errors) {
        User user;

        if (errors.hasErrors()) {
            errorList = new ArrayList<>();
            errors.getAllErrors().forEach(objectError -> {
                String msg = messageSource.getMessage(Objects.requireNonNull(objectError.getCode()), null, Locale.getDefault());
                errorList.add(msg);
            });
            logger.error("Field Error" + errorList.toString());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorList.toString());
        } else {
            try {
                user = RegistrationFormUtil.convert(registrationForm);
                userRepo.updateUser(user);
                logger.info("UserService method updateUser Successful");
            } catch (Exception e) {
                logger.error("UserService method updateUser Error Message : " + e.getMessage());
            }
        }
    }

    @Override
    public void deleteUser(long id) {
        try {
            userRepo.deleteUser(id);
            logger.info("UserService method deleteUser Successful");
        } catch (Exception e) {
            logger.error("UserService method deleteUser Error Message : " + e.getMessage());
        }
    }
}
