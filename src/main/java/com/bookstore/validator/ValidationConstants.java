package com.bookstore.validator;

public class ValidationConstants {
    public static final String NAME = "name";
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 500;

    public static final String SURNAME = "surname";
    public static final int SURNAME_MIN_LENGTH = 3;
    public static final int SURNAME_MAX_LENGTH = 50;

    public static final String USERNAME = "username";
    public static final int USERNAME_MIN_LENGTH = 3;
    public static final int USERNAME_MAX_LENGTH = 50;

    public static final String PASSWORD = "password";
    public static final int PASSWORD_MIN_LENGTH = 6;
}
