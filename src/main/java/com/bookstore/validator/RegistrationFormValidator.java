package com.bookstore.validator;

import com.bookstore.domain.RegistrationForm;
import com.bookstore.service.CommonService;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static com.bookstore.validator.ValidationConstants.*;

@Component
public class RegistrationFormValidator implements Validator {
    @Autowired
    CommonService commonService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(RegistrationForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegistrationForm form = (RegistrationForm) target;
        String regexAzName = "^[a-zA-ZöüƏəşŞçÇĞğÖÜ]+$";

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "registrationForm.name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "registrationForm.surName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "registrationForm.userName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "registrationForm.email.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "registrationForm.password.required");

        if (!errors.hasErrors()) {
            if (!GenericValidator.isInRange(form.getName().length(), NAME_MIN_LENGTH, NAME_MAX_LENGTH)) {
                errors.rejectValue("name", "registrationForm.name.length");
            }
            if (!GenericValidator.matchRegexp(form.getName(), regexAzName)) {
                errors.rejectValue("name", "registrationForm.name.invalid");
            }

            if (!GenericValidator.isInRange(form.getSurname().length(), SURNAME_MIN_LENGTH, SURNAME_MAX_LENGTH)) {
                errors.rejectValue("surname", "registrationForm.surName.length");
            }
            if (!GenericValidator.matchRegexp(form.getSurname(), regexAzName)) {
                errors.rejectValue("surname", "registrationForm.surName.invalid");
            }

            if (commonService.checkUserName(form.getUsername())) {
                errors.rejectValue("userName", "registrationForm.userName.duplicate");
            }
            if (!GenericValidator.isInRange(form.getUsername().length(), USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH)) {
                errors.rejectValue("username", "registrationForm.username.length");
            }

            if (!GenericValidator.isEmail(form.getEmail())) {
                errors.rejectValue("email", "registrationForm.email.invalid");
            }

            if (!GenericValidator.minValue(form.getPassword().length(), PASSWORD_MIN_LENGTH)) {
                errors.rejectValue("password", "registrationForm.password.length");
            }

        }


    }
}
