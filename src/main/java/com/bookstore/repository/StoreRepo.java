package com.bookstore.repository;

import com.bookstore.domain.Books;
import com.bookstore.domain.Store;

import java.util.List;
import java.util.Optional;

public interface StoreRepo {
    List<Store> getStoreList(int limit, int offset);

    List<Store> getStoreName();
    Optional<Store> getStoreListById(long id);

    List<Store> searchStoreList(String search, int start, int length);

    int countSearchStoreList(String search);

    Store addStore(Store store);

    void deleteStore(long id);

    void updateStore(Store store);

    Integer countStore();
}
