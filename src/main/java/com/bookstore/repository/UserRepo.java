package com.bookstore.repository;

import com.bookstore.domain.Books;
import com.bookstore.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserRepo {
    List<User> getUserList(int limit, int offset);

    Optional<User> getUserListById(long id);

    List<User> searchUserList(String search, int start, int length);

    int countSearchUserList(String search);

    Optional<User> getUserListByUsername(String username);

    User addUser(User user);

    void deleteUser(long id);
    void updateUser(User user);
    Integer countUser();
}
