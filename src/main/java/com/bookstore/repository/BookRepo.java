package com.bookstore.repository;

import com.bookstore.domain.Books;
import com.bookstore.domain.Store;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface BookRepo {
    List<Books> getBooksList(int limit, int offset);

    Optional<Books> getBookListById(long id);

    List<Books> searchBookList(String search,int start,int length);

    int countSearchBookList(String search);

    Books addBook(Books books);

    int deleteBook(long id);

    int updateBook(Books books);

    Integer countAllBook();

    Integer countBook();


}
