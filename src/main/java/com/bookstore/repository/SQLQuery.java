package com.bookstore.repository;

import org.apache.commons.beanutils.PropertyUtilsBean;

public class SQLQuery {
    public static final String GET_BOOK_LIST = "SELECT  " +
            "    b.book_id, " +
            "    b.book_name, " +
            "    b.book_author, " +
            "    b.book_publisher, " +
            "    b.book_year, " +
            "    b.book_page, " +
            "    b.book_price, " +
            "    s.store_id, " +
            "    s.store_name, " +
            "    s.store_address, " +
            "    s.idate, " +
            "    s.udate, " +
            "    b.idate, " +
            "    b.udate " +
            "FROM " +
            "    books b " +
            "        INNER JOIN " +
            "    store s ON b.store_id = s.store_id " +
            "        AND s.store_status = 1 " +
            "WHERE " +
            "    b.book_status = 1 " +
            "ORDER BY b.book_id ASC " +
            "LIMIT ? OFFSET ? ";

    public static final String GET_COUNT_BOOK_LIST = "SELECT  " +
            "   COUNT(book_id)  " +
            "FROM " +
            "    books  " +
            "    WHERE " +
            "    book_status = 1 ";

    public static final String GET_BOOK_LIST_BY_ID = "SELECT " +
            "    b.book_id, " +
            "    b.book_name, " +
            "    b.book_author, " +
            "    b.book_publisher, " +
            "    b.book_year, " +
            "    b.book_page, " +
            "    b.book_price, " +
            "    s.store_id, " +
            "    s.store_name, " +
            "    s.store_address, " +
            "    s.idate, " +
            "    s.udate, " +
            "    b.idate, " +
            "    b.udate " +
            " FROM " +
            "    books b " +
            "        INNER JOIN " +
            "    store s ON b.store_id = s.store_id " +
            "        AND s.store_status = 1 " +
            " WHERE " +
            "    b.book_status = 1 AND b.book_id = ? ";
    public static final String GET_BOOK_LIST_SEARCH = "SELECT " +
            "    b.book_id, " +
            "    b.book_name, " +
            "    b.book_author, " +
            "    b.book_publisher, " +
            "    b.book_year, " +
            "    b.book_page, " +
            "    b.book_price, " +
            "    s.store_id, " +
            "    s.store_name, " +
            "    s.store_address, " +
            "    s.idate, " +
            "    s.udate, " +
            "    b.idate, " +
            "    b.udate " +
            "FROM " +
            "    books b " +
            "        INNER JOIN " +
            "    store s ON b.store_id = s.store_id " +
            "        AND s.store_status = 1 " +
            "WHERE " +
            "   b.book_status = 1 AND " +
            "   CONCAT(" +
            "    b.book_id," +
            "    b.book_name," +
            "    b.book_author," +
            "    b.book_publisher," +
            "    b.book_year," +
            "    b.book_page," +
            "    b.book_price, " +
            "    s.store_id, " +
            "    s.store_name, " +
            "    s.store_address, " +
            "    s.idate, " +
            "    ifnull(s.udate,''), " +
            "    b.idate, " +
            "    ifnull(b.udate,'')  " +
            ") like binary ? " +
            "ORDER BY b.book_id ASC " +
            "LIMIT ?,  ? ";
    public static final String COUNT_BOOK_LIST_SEARCH = "SELECT " +
            "    COUNT(b.book_id) " +
            "FROM " +
            "    books b " +
            "        INNER JOIN " +
            "    store s ON b.store_id = s.store_id " +
            "        AND s.store_status = 1 " +
            "WHERE " +
            "    CONCAT(b.book_id, " +
            "            b.book_name, " +
            "            b.book_author, " +
            "            b.book_publisher, " +
            "            b.book_year, " +
            "            b.book_page, " +
            "            b.book_price, " +
            "            s.store_id, " +
            "            s.store_name, " +
            "            s.store_address, " +
            "            s.idate, " +
            "            IFNULL(s.udate, ''), " +
            "            b.idate, " +
            "            IFNULL(b.udate, '')) LIKE BINARY ?   " +
            "        AND b.book_status = 1 " +
            "ORDER BY b.book_id ASC";

    public static final String ADD_BOOK = "INSERT INTO books (" +
            "  books.book_name, " +
            "  books.book_author, " +
            "  books.book_publisher, " +
            "  books.book_year, " +
            "  books.book_page, " +
            "  books.book_price, " +
            "  books.store_id ) " +
            "  VALUES (?, ?, ?, ?, ?, ?, ? ) ";

    public static final String UPDATE_BOOK = "UPDATE books  " +
            "SET  " +
            "    book_name = ?, " +
            "    book_author = ?, " +
            "    book_publisher = ?, " +
            "    book_year = ?, " +
            "    book_page = ?, " +
            "    book_price = ?, " +
            "    store_id = ?, " +
            "    udate = CURRENT_TIMESTAMP " +
            "WHERE " +
            "    book_id = ? AND book_status=1";

    public static final String DELETE_BOOK = "UPDATE books SET book_status = 0, udate=CURRENT_TIMESTAMP WHERE book_id = ? ";


    public static final String GET_COUNT_BOOKS = "SELECT COUNT(book_id) FROM books WHERE book_status = 1 ";

    public static final String GET_COUNT_BOOK_BY_ID = "SELECT COUNT(book_id) FROM books WHERE book_status = 1 AND book_id = ? ";

    public static final String GET_USER_LIST = "SELECT " +
            "    user_id, " +
            "    user_name, " +
            "    user_surname, " +
            "    user_uname, " +
            "    user_email, " +
            "    user_password, " +
            "    user_role, " +
            "    idate, " +
            "    udate " +
            "FROM " +
            "    user " +
            "WHERE " +
            "    user_status = 1 " +
            "ORDER BY user_id ASC " +
            "LIMIT ? OFFSET ? ";
    public static final String GET_USER_LIST_BY_ID = "SELECT " +
            "    user_id, " +
            "    user_name, " +
            "    user_surname, " +
            "    user_uname, " +
            "    user_email, " +
            "    user_password, " +
            "    user_role, " +
            "    idate, " +
            "    udate " +
            "FROM " +
            "    user " +
            "WHERE " +
            "    user_status = 1 AND user_id = ? ";

    public static final String GET_USER_LIST_SEARCH="SELECT " +
            "    user_id, " +
            "    user_name, " +
            "    user_surname, " +
            "    user_uname, " +
            "    user_email, " +
            "    user_password, " +
            "    user_role, " +
            "    idate, " +
            "    udate " +
            "FROM " +
            "    user " +
            "WHERE " +
            "    user_status = 1 " +
            "   CONCAT(" +
            "    user_id, " +
            "    user_name, " +
            "    user_surname, " +
            "    user_uname, " +
            "    user_email, " +
            "    user_password, " +
            "    user_role, " +
            "    idate, " +
            "    ifnull(udate,'')  " +
            ") like binary ? " +
            "ORDER BY user_id ASC " +
            "LIMIT ? OFFSET ? ";

    public static final String COUNT_USER_LIST_SEARCH="SELECT " +
            "   COUNT(user_id)" +
            "FROM " +
            "    user " +
            "WHERE " +
            "    user_status = 1 " +
            "   CONCAT(" +
            "    user_id, " +
            "    user_name, " +
            "    user_surname, " +
            "    user_uname, " +
            "    user_email, " +
            "    user_password, " +
            "    user_role, " +
            "    idate, " +
            "    ifnull(udate,'')  " +
            ") like binary ? " +
            "ORDER BY user_id ASC " +
            "LIMIT ? OFFSET ? " ;
    public static final String GET_USER_LIST_BY_USERNAME = "SELECT " +
            "    user_id, " +
            "    user_name, " +
            "    user_surname, " +
            "    user_uname, " +
            "    user_email, " +
            "    user_password, " +
            "    user_role, " +
            "    idate, " +
            "    udate " +
            "FROM " +
            "    user " +
            "WHERE " +
            "    user_status = 1 AND user_uname = ? ";


    public static final String ADD_USER = "INSERT INTO user(name, surname, username, email, password, user_role) " +
            "VALUES (?, ?, ?, ?, ?, 1)";

    public static String UPDATE_USER = "UPDATE user " +
            "SET  " +
            "    user_name = ?, " +
            "    user_surname = ?, " +
            "    user_uname = ?, " +
            "    user_email = ?, " +
            "    user_password = ?, " +
            "    user_role = ?, " +
            "    udate = CURRENT_TIMESTAMP " +
            "WHERE " +
            "    user.user_id = ? " +
            "        AND user_status = 1";

    public static final String DELETE_USER = "UPDATE user  " +
            "SET " +
            "    user_status = 0, " +
            "WHERE " +
            "    user_id = ?";

    public static final String GET_COUNT_USER = "SELECT COUNT(user_id) FROM user WHERE user_status = 1";


    public static final String CHECK_USERNAME = "SELECT COUNT(user_id) FROM user WHERE user_name = ? AND user_status = 1 ";


    public static final String GET_STORE_LIST = "SELECT " +
            "    store_id, " +
            "    store_name, " +
            "    store_address, " +
            "    idate, " +
            "    udate" +
            "FROM" +
            "   store" +
            "WHERE" +
            "   store_status = 1 " +
            "ORDER BY store_id ASC " +
            "LIMIT ? OFFSET ?";
    public static final String GET_STORE_LIST_NAME = "SELECT  " +
            "    store_id, " +
            "    store_name, " +
            "    store_address, " +
            "    idate, " +
            "    udate " +
            " FROM " +
            "    store " +
            " WHERE " +
            "    store_status = 1 " +
            " ORDER BY store_id ASC ";

    public static final String GET_STORE_LIST_BY_ID = "SELECT " +
            "   store_name, " +
            "   store_address, " +
            "   idate, " +
            "   udate " +
            "FROM " +
            "  store " +
            "WHERE " +
            "  store_id = ? AND store_status = 1 ";

    public static final String GET_STORE_LIST_SEARCH="SELECT  " +
            "    store_id, " +
            "    store_name, " +
            "    store_address, " +
            "    idate, " +
            "    udate " +
            " FROM " +
            "    store " +
            " WHERE " +
            "    store_status = 1 " +
            "   CONCAT(" +
            "    store_id, " +
            "    store_name, " +
            "    store_address, " +
            "    idate, " +
            "    ifnull(udate,'')  " +
            ") like binary ? " +
            "ORDER BY store_id ASC " +
            "LIMIT ?,  ? ";

      public static final String COUNT_STORE_LIST_SEARCH="SELECT  " +
              "   COUNT(store_id)" +
              " FROM " +
              "    store " +
              " WHERE " +
              "    store_status = 1 " +
              "   CONCAT(" +
              "    store_id, " +
              "    store_name, " +
              "    store_address, " +
              "    idate, " +
              "    ifnull(udate,'')  " +
              ") like binary ? " +
              "ORDER BY store_id ASC " ;
    public static final String ADD_STORE = "INSERT INTO store(store_name,store_address)  " +
            "    VALUES (?, ? )";

    public static final String UPDATE_STORE = "UPDATE store " +
            "SET " +
            "    store_name = ? , " +
            "    store_address = ? " +
            "WHERE " +
            "    store_id = ? " +
            "        AND store.store_status = 1 ";

    public static final String DELETE_STORE = "UPDATE store " +
            "SET  " +
            "    store_status = 0 ," +
            "WHERE " +
            "    store_id = ? ";

    public static final String GET_COUNT_STORE = "SELECT " +
            "   COUNT(store_id) " +
            "FROM " +
            "    store " +
            "    WHERE " +
            "    store_status = 1 ";
}
