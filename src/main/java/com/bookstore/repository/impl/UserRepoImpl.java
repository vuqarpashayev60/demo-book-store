package com.bookstore.repository.impl;

import com.bookstore.domain.Books;
import com.bookstore.domain.User;
import com.bookstore.repository.SQLQuery;
import com.bookstore.repository.UserRepo;
import com.bookstore.rowmapper.UserRowMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class UserRepoImpl implements UserRepo {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    UserRowMapper userRowMapper;

    @Override
    public List<User> getUserList(int limit, int offset) {
            Object[] args = new Object[]{limit, offset};
            List<User> userList = jdbcTemplate.query(SQLQuery.GET_USER_LIST, args, userRowMapper);
            return userList;
    }

    @Override
    public Optional<User> getUserListById(long id) {
            Optional<User> optionalUser = Optional.empty();
            Object[] args = new Object[]{id};
            List<User> userList = jdbcTemplate.query(SQLQuery.GET_USER_LIST_BY_ID, userRowMapper, args);
            if (!userList.isEmpty()) {
                optionalUser = userList.stream().findFirst();
            }
            return optionalUser;
    }

    @Override
    public List<User> searchUserList(String search, int start, int length) {
        Object[] args = new Object[]{"%" + search + "%", start, length};
        return jdbcTemplate.query(SQLQuery.GET_USER_LIST_SEARCH, args, userRowMapper);
    }

    @Override
    public int countSearchUserList(String search) {
        Object[] args = new Object[]{"%" + search + "%"};
        return jdbcTemplate.queryForObject(SQLQuery.COUNT_USER_LIST_SEARCH, args, Integer.class);
    }

    @Override
    public Optional<User> getUserListByUsername(String username) {
            Optional<User> optionalUser = Optional.empty();
            Object[] args = new Object[]{username};
            List<User> userList = jdbcTemplate.query(SQLQuery.GET_USER_LIST_BY_USERNAME, userRowMapper, args);
            if (!userList.isEmpty()) {
                optionalUser = userList.stream().findFirst();
            }
            return optionalUser;
    }

    @Override
    public User addUser(User user) {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(con -> {
                PreparedStatement ps = con.prepareStatement(SQLQuery.ADD_USER, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, user.getName());
                ps.setString(2, user.getSurname());
                ps.setString(3, user.getUsername());
                ps.setString(4, user.getEmail());
                ps.setString(5, user.getPassword());
                return ps;
            }, keyHolder);
            user.setId(Objects.requireNonNull(keyHolder.getKey()).longValue());
            return user;
    }

    @Override
    public void deleteUser(long id) {
            Object[] args = new Object[]{id};
            jdbcTemplate.update(SQLQuery.DELETE_USER, args);
    }

    @Override
    public void updateUser(User user) {
            Object[] args = new Object[]{user.getName(), user.getSurname(), user.getUsername(), user.getEmail(), user.getPassword(), user.getUserRole().getId()};
            jdbcTemplate.update(SQLQuery.UPDATE_USER, args);
    }

    @Override
    public Integer countUser() {
        return jdbcTemplate.queryForObject(SQLQuery.GET_COUNT_USER, Integer.class);
    }
}
