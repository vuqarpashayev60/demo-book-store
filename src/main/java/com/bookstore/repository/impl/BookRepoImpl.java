package com.bookstore.repository.impl;

import com.bookstore.domain.Books;
import com.bookstore.repository.BookRepo;
import com.bookstore.repository.SQLQuery;
import com.bookstore.rowmapper.BookRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class BookRepoImpl implements BookRepo {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    BookRowMapper bookRowMapper;

    @Override
    public List<Books> getBooksList(int limit, int offset) {
        Object[] args = new Object[]{limit, offset};
        List<Books> booksList = jdbcTemplate.query(SQLQuery.GET_BOOK_LIST, args, bookRowMapper);
        return booksList;
    }

    @Override
    public Optional<Books> getBookListById(long bookId) {
        Optional<Books> optionalBooks = Optional.empty();
        Object[] args = new Object[]{bookId};
        List<Books> books = jdbcTemplate.query(SQLQuery.GET_BOOK_LIST_BY_ID, bookRowMapper, args);
        if (!books.isEmpty()) {
            optionalBooks = books.stream().findFirst();
        }
        return optionalBooks;
    }

    @Override
    public List<Books> searchBookList(String search, int start, int length) {
        Object[] args = new Object[]{"%" + search + "%", start, length};
        return jdbcTemplate.query(SQLQuery.GET_BOOK_LIST_SEARCH, args, bookRowMapper);
    }

    @Override
    public int countSearchBookList(String search) {
        Object[] args = new Object[]{"%" + search + "%"};
        return jdbcTemplate.queryForObject(SQLQuery.COUNT_BOOK_LIST_SEARCH, args, Integer.class);
    }

    @Override
    public Books addBook(Books books) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(con -> {
            PreparedStatement prs = con.prepareStatement(SQLQuery.ADD_BOOK, Statement.RETURN_GENERATED_KEYS);
            prs.setString(1, books.getName());
            prs.setString(2, books.getAuthor());
            prs.setString(3, books.getPublisher());
            prs.setInt(4, books.getYear());
            prs.setInt(5, books.getPage());
            prs.setBigDecimal(6, books.getPrice());
            prs.setLong(7, books.getStore().getId());
            return prs;
        }, keyHolder);
        books.setId(Objects.requireNonNull(keyHolder.getKey()).longValue());
        return books;
    }

    @Override
    public int deleteBook(long id) {
        Object[] args = new Object[]{id};
        return jdbcTemplate.update(SQLQuery.DELETE_BOOK, args);
    }

    @Override
    public int updateBook(Books books) {
        Object[] args = new Object[]{books.getName(), books.getAuthor(), books.getPublisher(), books.getYear(), books.getPage(), books.getPrice(), books.getStore().getId(), books.getId()};
        return jdbcTemplate.update(SQLQuery.UPDATE_BOOK, args);
    }

    @Override
    public Integer countAllBook() {
        return jdbcTemplate.queryForObject(SQLQuery.GET_COUNT_BOOKS, Integer.class);
    }

    @Override
    public Integer countBook() {
        return jdbcTemplate.queryForObject(SQLQuery.GET_COUNT_BOOK_LIST, Integer.class);
    }
}
