package com.bookstore.repository.impl;

import com.bookstore.domain.Store;
import com.bookstore.repository.SQLQuery;
import com.bookstore.repository.StoreRepo;
import com.bookstore.rowmapper.StoreRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class StoreRepoImpl implements StoreRepo {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private StoreRowMapper storeRowMapper;

    @Override
    public List<Store> getStoreList(int limit, int offset) {
        Object[] args = new Object[]{limit, offset};
        return jdbcTemplate.query(SQLQuery.GET_STORE_LIST, args, storeRowMapper);
    }

    @Override
    public List<Store> getStoreName() {
        return jdbcTemplate.query(SQLQuery.GET_STORE_LIST_NAME, storeRowMapper);
    }

    @Override
    public Optional<Store> getStoreListById(long id) {
        Optional<Store> optionalStore = Optional.empty();
        Object[] args = new Object[]{id};
        List<Store> storeList = jdbcTemplate.query(SQLQuery.GET_STORE_LIST_BY_ID, storeRowMapper, args);
        if (!storeList.isEmpty()) {
            optionalStore = storeList.stream().findFirst();
        }
        return optionalStore;
    }

    @Override
    public List<Store> searchStoreList(String search, int start, int length) {
        Object[] args = new Object[]{"%" + search + "%", start, length};
        return jdbcTemplate.query(SQLQuery.GET_STORE_LIST_SEARCH, args, storeRowMapper);
    }

    @Override
    public int countSearchStoreList(String search) {
        Object[] args = new Object[]{"%" + search + "%"};
        return jdbcTemplate.queryForObject(SQLQuery.COUNT_STORE_LIST_SEARCH, args, Integer.class);
    }

    @Override
    public Store addStore(Store store) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(con -> {
            PreparedStatement prs = con.prepareStatement(SQLQuery.ADD_STORE, Statement.RETURN_GENERATED_KEYS);
            prs.setString(1, store.getName());
            prs.setString(2, store.getAddress());
            return prs;
        }, keyHolder);
        store.setId(Objects.requireNonNull(keyHolder.getKey()).longValue());
        return store;
    }

    @Override
    public void deleteStore(long id) {
        Object[] args = new Object[]{id};
        jdbcTemplate.update(SQLQuery.DELETE_STORE, args);
    }

    @Override
    public void updateStore(Store store) {
        Object args = new Object[]{store.getName(), store.getAddress()};
        jdbcTemplate.update(SQLQuery.UPDATE_STORE, args);
    }

    @Override
    public Integer countStore() {
        return jdbcTemplate.queryForObject(SQLQuery.GET_COUNT_STORE, Integer.class);
    }
}
