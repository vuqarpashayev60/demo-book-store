package com.bookstore.repository.impl;

import com.bookstore.repository.CommonRepo;
import com.bookstore.repository.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CommonRepoImpl implements CommonRepo {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public boolean checkUserName(String userName) {
        Object[] args = new Object[]{userName};
        int count = jdbcTemplate.queryForObject(SQLQuery.CHECK_USERNAME,args, Integer.class);
        return count > 0;
    }
}
