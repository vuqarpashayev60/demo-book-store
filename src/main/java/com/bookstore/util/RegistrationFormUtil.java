package com.bookstore.util;

import com.bookstore.domain.RegistrationForm;
import com.bookstore.domain.User;

public class RegistrationFormUtil {
    public static User convert(RegistrationForm registrationForm){
        User user=new User();
        user.setName(registrationForm.getName());
        user.setSurname(registrationForm.getSurname());
        user.setUsername(registrationForm.getUsername());
        user.setUsername(registrationForm.getUsername());
        user.setEmail(registrationForm.getEmail());
        user.setPassword(registrationForm.getPassword());
        user.setUserRole(registrationForm.getUserRole());
        return user;
    }
}
