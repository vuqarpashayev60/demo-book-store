package com.bookstore.security;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class BookStoreFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        System.out.println("Error Message Is : " + exception.getMessage());
        if (exception instanceof BadCredentialsException) {
            response.sendRedirect(request.getContextPath() + "/common/login?error");
        } else if (exception instanceof DisabledException) {
            //Eger userin vaxti bitibse
            response.sendRedirect(request.getContextPath()+"");
        }
    }
}
