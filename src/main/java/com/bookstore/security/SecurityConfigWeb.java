package com.bookstore.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecurityConfigWeb extends WebSecurityConfigurerAdapter {
    @Autowired
    private BookStoreSuccessHandler bookStoreSuccessHandler;

    @Autowired
    private BookStoreFailureHandler bookStoreFailureHandler;

    @Autowired
    BookStoreUserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //setup Authentication for web application don't api
//        auth.inMemoryAuthentication()
//                .withUser("admin")
//                .password("admin")
//                .roles("ADMIN")
//                .and()
//                .withUser("robot")
//                .password("xala")
//                .roles("USER");
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //Setup Autorization for web application don't api
        http
                .authorizeRequests()
                .antMatchers("/store/","/store/add", "/store/update", "/store/delete/{storeId}", "/user/**").hasRole("ADMIN")
                .antMatchers("/add", "/update", "/delete","/user/**").hasAnyRole("USER","ADMIN")
                .antMatchers("/","/common/**","/api/**").permitAll()
                .and()
                .formLogin()
//                .disable()   // For form disable
//                .csrf().disable()  // /update and /delete and /add direct isn't working when csrf is active
                .loginPage("/common/login").permitAll()
                .successHandler(bookStoreSuccessHandler)
               .failureHandler(bookStoreFailureHandler)
                .and()
                .rememberMe()
                .userDetailsService(userDetailsService);
//                .successForwardUrl("/")  //Default  success page if you be login
//                .failureForwardUrl("/error")   //Default error page if you can't be login
                //.defaultSuccessUrl("/");      //go to welcome when login is successful
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(gePasswordEncoder());
        return authenticationProvider;
    }

    @Bean
    PasswordEncoder gePasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
