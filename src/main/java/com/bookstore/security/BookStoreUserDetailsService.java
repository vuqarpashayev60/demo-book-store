package com.bookstore.security;

import com.bookstore.domain.User;
import com.bookstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookStoreUserDetailsService implements UserDetailsService {
    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserPrincipal userPrincipal = null;
        Optional<User> userOptional = userService.getUserListByUsername(username);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            userPrincipal = new UserPrincipal(user);
        } else {
            throw new UsernameNotFoundException("User " + username + "not found");
        }
        return userPrincipal;
    }
}
