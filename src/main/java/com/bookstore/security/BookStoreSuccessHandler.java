package com.bookstore.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class BookStoreSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        UserPrincipal userPrincipal= (UserPrincipal) authentication.getPrincipal();
        System.out.println("User success login : "+userPrincipal.getUser());

        String page=userPrincipal.getUser().getUserRole().getSuccesspage();
        System.out.println("Redirecting Page : "+page);
        response.sendRedirect(request.getContextPath()+page);
    }
}
