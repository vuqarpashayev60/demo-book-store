package com.bookstore.rowmapper;

import com.bookstore.domain.Store;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class StoreRowMapper implements RowMapper<Store> {
    @Override
    public Store mapRow(ResultSet rs, int rowNum) throws SQLException {
        Store store=new Store();
        store.setId(rs.getLong("store_id"));
        store.setName(rs.getString("store_name"));
        store.setAddress(rs.getString("store_address"));
        store.setIdate(rs.getTimestamp("idate").toLocalDateTime());
        if (rs.getTimestamp("udate") != null) {
            store.setUdate(rs.getTimestamp("udate").toLocalDateTime());
        }
        return store;
    }
}
