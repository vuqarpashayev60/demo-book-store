package com.bookstore.rowmapper;

import com.bookstore.domain.Books;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BookRowMapper implements RowMapper<Books> {
    @Override
    public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
        Books books = new Books();
        books.setId(rs.getLong("b.book_id"));
        books.setName(rs.getString("b.book_name"));
        books.setAuthor(rs.getString("b.book_author"));
        books.setPublisher(rs.getString("b.book_publisher"));
        books.setYear(rs.getInt("b.book_year"));
        books.setPage(rs.getInt("b.book_page"));
        books.setPrice(rs.getBigDecimal("b.book_price"));
        books.getStore().setId(rs.getLong("s.store_id"));
        books.getStore().setName(rs.getString("s.store_name"));
        books.getStore().setAddress(rs.getString("s.store_address"));
        books.getStore().setIdate(rs.getTimestamp("s.idate").toLocalDateTime());
        if (rs.getTimestamp("udate") != null) {
            books.getStore().setUdate(rs.getTimestamp("s.udate").toLocalDateTime());
        }
        books.setIdate(rs.getTimestamp("b.idate").toLocalDateTime());
        if (rs.getTimestamp("udate") != null) {
            books.setUdate(rs.getTimestamp("b.udate").toLocalDateTime());
        }
        return books;
    }
}
