package com.bookstore.rowmapper;

import com.bookstore.constant.UserRole;
import com.bookstore.domain.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User user = new User();
        user.setId(rs.getLong("user_id"));
        user.setName(rs.getString("user_name"));
        user.setSurname(rs.getString("user_surname"));
        user.setUsername(rs.getString("user_uname"));
        user.setEmail(rs.getString("user_email"));
        user.setPassword(rs.getString("user_password"));
        user.setUserRole(UserRole.from(rs.getInt("user_role")));
        user.setIdate(rs.getTimestamp("idate").toLocalDateTime());
        if (rs.getTimestamp("udate") != null) {
            user.setUdate(rs.getTimestamp("udate").toLocalDateTime());
        }
        return user;
    }
}
