package com.bookstore.controller.api;

import com.bookstore.domain.DataTable;
import com.bookstore.domain.Store;
import com.bookstore.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/store")
public class StoreController {

    @Autowired
    StoreService storeService;

    @GetMapping("/list")
    public DataTable getStoreListDataTable(
            @RequestParam(name = "draw") int draw,
            @RequestParam(name = "start", required = false, defaultValue = "0") int start,
            @RequestParam(name = "length", required = false, defaultValue = "10") int length,
            @RequestParam(name = "search[value]", required = false, defaultValue = "") String search,
            @RequestParam(name = "order[0][column]", required = false, defaultValue = "0") int sortColumn,
            @RequestParam(name = "order[0][dir]", required = false, defaultValue = "asc") String sortDirection
    ) {
        return storeService.getStoreDataTable(draw, start, length, search, sortColumn, sortDirection);
    }

    @GetMapping("/name")
    public List<Store> getAllStoreName() {
        return storeService.getStoreName();
    }

    @GetMapping("/list/{storeId}")
    public List<Store> storeListById(@PathVariable long storeId) {
        return storeService.getStoreListById(storeId);
    }

    @PostMapping("/add")
    public Store storeAdd(@RequestBody Store store) {
        return storeService.addStore(store);

    }

    @PutMapping("/update")
    public void storeUpdate(@RequestBody Store store) {
        storeService.updateStore(store);
    }

    @DeleteMapping("/delete/{storeId}")
    public void storeDelete(@PathVariable long storeId) {
        storeService.deleteStore(storeId);
    }
}
