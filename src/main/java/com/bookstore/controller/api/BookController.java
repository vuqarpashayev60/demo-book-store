package com.bookstore.controller.api;

import com.bookstore.domain.Books;
import com.bookstore.domain.DataTable;
import com.bookstore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api")
public class BookController {

    @Autowired
    BookService bookService;

    @GetMapping("/")   //For JQUERY
    public DataTable getBookListDataTable(
            @RequestParam(name = "draw") int draw,
            @RequestParam(name = "start", required = false, defaultValue = "0") int start,
            @RequestParam(name = "length", required = false, defaultValue = "10") int length,
            @RequestParam(name = "search[value]", required = false, defaultValue = "") String search,
            @RequestParam(name = "order[0][column]", required = false, defaultValue = "0") int sortColumn,
            @RequestParam(name = "order[0][dir]", required = false, defaultValue = "asc") String sortDirection
    ) {
        return bookService.getBookDataTable(draw, start, length, search, sortColumn, sortDirection);
    }

    @GetMapping("/{bookId}")
    public List<Books> getBooksListById(@PathVariable long bookId) {
        return bookService.getBookListById(bookId);
    }

    @PostMapping("/add")
    public Books bookAdd(@RequestBody Books books) {
        return bookService.addBook(books);
    }

    @PutMapping("/update")
    public void bookUpdate(@RequestBody Books books) {
        bookService.updateBook(books);
    }

    @DeleteMapping("/delete/{bookId}")
    public void deleteBook(@PathVariable long bookId) {
        bookService.deleteBook(bookId);
    }
}
