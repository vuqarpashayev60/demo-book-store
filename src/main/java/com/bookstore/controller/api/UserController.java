package com.bookstore.controller.api;

import com.bookstore.domain.DataTable;
import com.bookstore.domain.RegistrationForm;
import com.bookstore.domain.User;
import com.bookstore.service.UserService;
import com.bookstore.validator.RegistrationFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RegistrationFormValidator registrationFormValidator;


    @InitBinder("registrationForm")
    public void initBinder(WebDataBinder dataBinder) {
        if (Objects.requireNonNull(dataBinder.getTarget()).getClass() == RegistrationForm.class) {
            dataBinder.setValidator(registrationFormValidator);
        }
    }

    @GetMapping("/list")
    public DataTable getStoreListDataTable(
            @RequestParam(name = "draw") int draw,
            @RequestParam(name = "start", required = false, defaultValue = "0") int start,
            @RequestParam(name = "length", required = false, defaultValue = "10") int length,
            @RequestParam(name = "search[value]", required = false, defaultValue = "") String search,
            @RequestParam(name = "order[0][column]", required = false, defaultValue = "0") int sortColumn,
            @RequestParam(name = "order[0][dir]", required = false, defaultValue = "asc") String sortDirection
    ) {
        return userService.getUserDataTable(draw, start, length, search, sortColumn, sortDirection);
    }

    @GetMapping("/list/{userId}")
    public List<User> getUserListById(@PathVariable long userId) {
        return userService.getUserListById(userId);
    }



    @PutMapping("/update")
    public void updateUser(@Validated @RequestBody RegistrationForm registrationForm, BindingResult errors) {
        userService.updateUser(registrationForm, errors);
    }

    @DeleteMapping(value = "/delete/{userId}")
    public void deleteUser(@PathVariable long userId) {
        userService.deleteUser(userId);
    }
}
