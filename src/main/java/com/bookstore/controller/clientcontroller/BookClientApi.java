package com.bookstore.controller.clientcontroller;

import com.bookstore.apiclient.BookStoreAPIClient;
import com.bookstore.apiclient.ResourceNotFound;
import com.bookstore.domain.Books;
import com.bookstore.domain.PaginationResult;
import com.bookstore.domain.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.util.Optional;

@RequestMapping("/client")
@Controller
public class BookClientApi {
    @Autowired
    private BookStoreAPIClient bookStoreAPIClient;

    ModelAndView mav;

    @GetMapping("/")
    public ModelAndView index() {
        int limit = 10;
        int offset = 0;
        mav = new ModelAndView("/web/book/book-list");
        PaginationResult<Books> paginationResult = bookStoreAPIClient.getBookList(limit, offset);
        mav.addObject("paginationResult", paginationResult);
        return mav;
    }

    @GetMapping("/{id}")
    public ModelAndView listById(@PathVariable(name = "id") long id) {
        mav = new ModelAndView("/web/book/book-details");
        try {
            Optional<Books> optionalBooks = bookStoreAPIClient.getBookListById(id);
            if (optionalBooks.isPresent()) {
                Books books = optionalBooks.get();
                mav.addObject("books", books);
            }
        } catch (ResourceNotFound e) {

        } catch (Exception e) {
            e.printStackTrace();
            mav.setViewName("error");
        }
        return mav;
    }

    @GetMapping("/edit/{id}")
    public ModelAndView listEdit(@PathVariable(name = "id") long id) {
        mav = new ModelAndView("/web/book/book-edit");
        try {
            Optional<Books> optionalBooks = bookStoreAPIClient.getBookListById(id);
            if (optionalBooks.isPresent()) {
                Books books = optionalBooks.get();
                mav.addObject("books", books);
            }
        } catch (ResourceNotFound e) {

        } catch (Exception e) {
            e.printStackTrace();
            mav.setViewName("error");
        }
        return mav;
    }

    @PostMapping("/update")
    public ModelAndView updateItem(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "author") String author,
            @RequestParam(name = "publisher") String publisher,
            @RequestParam(name = "year") int year,
            @RequestParam(name = "page") int page,
            @RequestParam(name = "price") BigDecimal price,
            @RequestParam(name = "storeName") String storeName
    ) {
        mav = new ModelAndView("redirect:/client/");
        Books books = new Books();
        books.setName(name);
        books.setAuthor(author);
        books.setPublisher(publisher);
        books.setYear(year);
        books.setPage(page);
        books.setPrice(price);
        books.getStore().setName(storeName);
        try {
            bookStoreAPIClient.updateBook(books);
        } catch (Exception e) {
            mav.setViewName("error");
        }
        return mav;
    }
}
