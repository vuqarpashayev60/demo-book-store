package com.bookstore.controller.web;

import com.bookstore.domain.RegistrationForm;
import com.bookstore.domain.User;
import com.bookstore.service.UserService;
import com.bookstore.validator.RegistrationFormValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Controller
@RequestMapping("/common")
public class CommonWebController {
    private static final Logger logger = Logger.getLogger(UserWebController.class);

    @Autowired
    UserService userService;

    @Autowired
    RegistrationFormValidator registrationFormValidator;


    @InitBinder("registrationForm")
    public void initBinder(WebDataBinder dataBinder) {
        if (Objects.requireNonNull(dataBinder.getTarget()).getClass() == RegistrationForm.class) {
            dataBinder.setValidator(registrationFormValidator);
        }
    }

    @Autowired
    User user;

    @GetMapping("/login")
    public String login() {
        System.out.println("Message : Login Page");
        return "web/login";
    }

    @GetMapping("/welcome")
    public String welcome() {
        System.out.println("Message : Welcome Page");
        return "/web/welcome";
    }

    @GetMapping("/error")
    public String error(){
        return "/web/error";
    }

    @GetMapping("/logout")
    public  String logOut(HttpServletRequest request){
        request.getSession().invalidate();
        return "web/login";
    }
    @GetMapping("/register")
    public  ModelAndView register(){
        ModelAndView mav = new ModelAndView("/web/register");

        RegistrationForm form = new RegistrationForm();
        mav.addObject("registrationForm", form);
        return mav;
    }
    @PostMapping("/registerus")
    public User userAdd(@Validated @RequestBody RegistrationForm registrationForm, BindingResult errors) {
        System.out.println("Registration Form : "+registrationForm);
        System.out.println("User Controller ADD");
        return userService.addUser(registrationForm, errors);
    }

//    @PostMapping("/logging")
//    public ModelAndView login(@RequestParam("username") String usrname,
//                              @RequestParam("password") String password,
//                              HttpSession session,
//                              RedirectAttributes redirectAttributes) {
//        System.out.println("UserName : " + usrname);
//        System.out.println("Password : " + password);
//        System.out.println("Deneme 1 2");
//        ModelAndView mv = new ModelAndView("redirect:/login");
//        String message;
//
//
//        Optional<User> userOpt = userService.getUserListByUsername(usrname);
//
//        if (userOpt.isPresent()) {
//            User user = userOpt.get();
//
//            if (BCrypt.checkpw(password, user.getPassword())) {
//                logger.debug("user role = " + user.getUserRole());
//                session.setAttribute("user", user);
//                mv.setViewName(user.getUserRole().getSuccesspage());
//            } else {
//                logger.error("Username is not found");
//            }
//        } else {
//            message = "Username or Password Is Incorrect";
//            //   redirectAttributes.addFlashAttribute("message", message);
//        }
//
//        return mv;
//    }
}
