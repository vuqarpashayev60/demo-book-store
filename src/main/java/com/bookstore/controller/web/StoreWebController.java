package com.bookstore.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/store")
public class StoreWebController {

    @GetMapping("/")
    public String storelist() {
        return "/web/store/store-list";
    }
}
