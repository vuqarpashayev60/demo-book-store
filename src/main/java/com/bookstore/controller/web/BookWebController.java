package com.bookstore.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class BookWebController {
    @GetMapping("/")
    public String booklist() {
        return "/web/book/book-list";
    }

}
