package com.bookstore.apiclient;

import com.bookstore.domain.Books;
import com.bookstore.domain.DataTable;
import com.bookstore.domain.PaginationResult;

import java.util.Optional;

public interface BookStoreAPIClient {
    PaginationResult<Books> getBookList(int limit, int offset);

    DataTable getDataTable(DataTable dataTable);

    Optional<Books> getBookListById(long id);

    Books addBook(Books books);

    boolean deleteBook(long id);

    boolean updateBook(Books books);
}
