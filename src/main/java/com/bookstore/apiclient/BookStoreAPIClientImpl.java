package com.bookstore.apiclient;

import com.bookstore.domain.Books;
import com.bookstore.domain.DataTable;
import com.bookstore.domain.PaginationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Component
public class BookStoreAPIClientImpl implements BookStoreAPIClient {
    @Value("${api.url}")
    private String apiUrl;

    @Autowired
    private BookStoreClientErrorHandler errorHandler;

    @Override
    public PaginationResult<Books> getBookList(int limit, int offset) {
        PaginationResult<Books> paginationResult;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(errorHandler);
        List<Books> booksList = restTemplate.getForObject(apiUrl + "/", List.class);
        paginationResult = null;
        return paginationResult;
    }

    @Override
    public DataTable getDataTable(DataTable dataTable) {
        return null;
    }

    @Override
    public Optional<Books> getBookListById(long id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(errorHandler);
        Optional<Books> optionalBooks = Optional.empty();
        Books books = restTemplate.getForObject("/" + id, Books.class);
        if (books != null) {
            optionalBooks = optionalBooks.of(books);
        }
        return optionalBooks;
    }

    @Override
    public Books addBook(Books books) {
        return null;
    }

    @Override
    public boolean deleteBook(long id) {
        return false;
    }

    @Override
    public boolean updateBook(Books books) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(errorHandler);
        restTemplate.put("/update", books);
        return true;
    }
}
