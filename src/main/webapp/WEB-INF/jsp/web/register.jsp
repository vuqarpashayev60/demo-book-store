<%--
  Created by IntelliJ IDEA.
  User: vuqar
  Date: 01.09.22
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <style>
        .center {
            margin: auto;
            width: 60%;
        }

        .form {
            border: 3px solid #73AD21;
            margin: auto;
            width: 30%;
            padding: 13%;
        }
    </style>
    <title>Regiter</title>
</head>
<body>
<div class="center">

    <form:form modelAttribute="registrationForm" method="post" action="registerus">
        <div class="form">
            <h1>User Register</h1>
            <br/>

            <label for="name">Name </label>
            <form:input path="name" id="name" type="text" placeholder="Name" name="name"/><br/><br/>
            <form:errors path="name"/>

            <label for="surname">Surname </label>
            <form:input path="surname" id="surname" type="text" placeholder="Surname" name="surname"/><br/><br/>
            <form:errors path="surname"/>

            <label for="username">Username </label>
            <form:input path="username" id="username" type="text" placeholder="UserName" name="username"/><br/><br/>
            <form:errors path="username"/>

            <label for="email">Email </label>
            <form:input path="email" id="email" type="email" placeholder="Email" name="email"/><br/><br/>
            <form:errors path="email"/>

            <label for="password">Password* </label>
            <form:input path="password" id="password" type="text" placeholder="Password"
                        name="password"/> <br/><br/>
            <form:errors path="password"/>

            <input type="submit" value="Save">
            <input type="reset" value="Clear"> <br/><br>
        </div>
    </form:form>
</div>

</body>
</html>
