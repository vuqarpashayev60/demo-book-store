<%--
  Created by IntelliJ IDEA.
  User: vuqar
  Date: 24.08.22
  Time: 12:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <style>
        .center {
            margin: auto;
            width: 60%;
        }

        .form {
            border: 3px solid #73AD21;
            margin: auto;
            width: 30%;
            padding: 25%;
        }
    </style>
    <title>Login</title>
</head>
<body>
<div class="center">
    <form method="post" action="login">
        <div class="form">
            <c:if test="${param.error != null}">
                Username or password is invalid!
            </c:if>

            <c:if test="${param.logout != null}">
                Successfully logged out!
            </c:if>
            <br/><br/>

            <label for="username">Username </label>
            <input id="username" type="text" placeholder="UserName" name="username"><br/><br/>

            <label for="password">Password* </label>
            <input id="password" type="password" placeholder="Password"
                   name="password"> <br/><br/>
            <input type="submit" value="Login"> <br/><br>

            <label for="remember">Remember Me</label>
            <input type="checkbox" name="remember-me" id="remember">

            <input type="hidden" 6
                   name="${_csrf.parameterName}"
                   value="${_csrf.token}"/>
        </div>
    </form>
</div>

</body>
</html>
