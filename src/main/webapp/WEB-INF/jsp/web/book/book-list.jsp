<%--
  Created by IntelliJ IDEA.
  User: vuqar
  Date: 14.08.22
  Time: 13:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Book List Page</title>
    <style>
        table, th, td {
            border: 1px solid;
        }
        .topcorner{
            position:absolute;
            top:0;
            right:0;
        }
        a.button {
            -webkit-appearance: button;
            -moz-appearance: button;
            appearance: button;

            text-decoration: none;
            color: initial;
        }
    </style>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">


</head>
<body>
<div class="topcorner" >
    <a class="button" style="color: dodgerblue" href="/common/login">Login</a>
    <a class="button"  style="color: #73AD21" href="/common/register">Register</a>
</div>
<h1>Book List</h1>
<button onclick="show_book_add()"><h3>Add New Book</h3></button>
<hr/>
<br/>

<table id="book-list">
    <thead>
    <tr>
        <th>Number</th>
        <th>Name</th>
        <th>Author</th>
        <th>Publisher</th>
        <th>Year</th>
        <th>Page</th>
        <th>Price</th>
        <th>Store</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody></tbody>
</table>
<br/>
<div id="book-details" style="display: none">
    <hr/>
    ID: <span id="id"></span> <br/>
    Name: <span id="name"></span><br/>
    Author: <span id="author"></span><br/>
    Publisher: <span id="publisher"></span><br/>
    Year : <span id="year"></span><br/>
    Page : <span id="page"></span><br/>
    Price : <span id="price"></span><br/>
    Store : <span id="store"></span>
</div>
<div id="book-add" style="display: none">
    <form>
        Name:<input type="text" id="add_name"><br/>
        Author:<input type="text" id="add_author"><br/>
        Publisher:<input type="text" id="add_publisher"><br/>
        Year:<input type="text" id="add_year"><br/>
        Page:<input type="text" id="add_page"><br/>
        Price:<input type="number" id="add_price" step="0.01"><br/>
        Store:<select id="add_store"> </select>
        <input type="hidden" 6
               name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
</div>
<div id="book-update" style="display: none">
    <form>
        Name:<input type="text" id="upd_name"><br/>
        Author:<input type="text" id="upd_author"><br/>
        Publisher:<input type="text" id="upd_publisher"><br/>
        Year:<input type="text" id="upd_year"><br/>
        Page:<input type="text" id="upd_page"><br/>
        Price:<input type="number" id="upd_price" step="0.01"><br/>
        Store:<select id="upd_store"> </select>
        <input type="hidden" id="id_book">
        <input type="hidden" 6
               name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
</div>
<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>
</body>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script>
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    function get_store_list() {
        $.ajax({
            url: '/api/store/name',
            success: function (data) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    $('#add_store').append("<option value=" + data[i].id +
                        ">" + data[i].name + "</option>");
                }
            }
        });
    }

    function show_book_add() {
        get_store_list();
        $('#book-add').dialog("open");

    }

    function setup_dialog() {
        $('#book-details').dialog({
            autoOpen: false,
            height: 250,
            width: 350,
            modal: true
        });
        $('#book-add').dialog({
            autoOpen: false,
            height: 350,
            width: 450,
            modal: true,
            buttons: {
                "Save": function () {
                    console.log("Add Save Function Intro ");
                    var table = $('#book-list').DataTable();
                    $(document).ajaxSend(function (e, xhr, options) {
                        xhr.setRequestHeader(header, token);
                    });
                        var item = {
                            "name": $('#add_name').val(),
                            "author": $('#add_author').val(),
                            "publisher": $('#add_publisher').val(),
                            "year": $('#add_year').val(),
                            "page": $('#add_page').val(),
                            "price": $('#add_price').val(),
                            "store":
                                {
                                    "id": $('#add_store').val()
                                }
                        };
                        var json = JSON.stringify(item);
                        console.log("JSON Data : "+json);
                        $.ajax({
                            url:'/api/add',
                            contentType: 'application/json',
                            method: 'POST',
                            data: json,
                            success: function (data, textStatus) {
                                $('#book-add').dialog("close");
                                table.ajax.reload(function (json) {
                                    $('#myInput').val(json.lastInput);
                                });
                                alert("Success Add Function");
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $('#book-add').dialog("close");
                                alert(textStatus + ' ' + jqXHR.status);
                            }
                        });
                },
                "Cancel": function () {
                    $('#book-add').dialog("close");
                }
            }
        });
        $('#book-update').dialog({
            autoOpen: false,
            height: 315,
            width: 450,
            modal: true,
            buttons: {
                "Save": function () {
                    var table = $('#book-list').DataTable();
                    $(document).ajaxSend(function (e, xhr, options) {
                        xhr.setRequestHeader(header, token);
                    });
                    var item = {
                        "name": $('#upd_name').val(),
                        "author": $('#upd_author').val(),
                        "publisher": $('#upd_publisher').val(),
                        "year": $('#upd_year').val(),
                        "page": $('#upd_page').val(),
                        "price": $('#upd_price').val(),
                        "store":
                            {
                                "id": $('#upd_store').val()
                            },
                        "id": $('#id_book').val()
                    };
                    var json = JSON.stringify(item);
                    console.log("Json Data : " + json);
                    $.ajax({
                        url: '/api/update',
                        contentType: 'application/json',
                        method: 'PUT',
                        data: json,
                        success: function (data, textStatus) {
                            $('#book-update').dialog("close");
                            table.ajax.reload(function (json) {
                                $('#myInput').val(json.lastInput);
                            });
                            alert("Success Update Function");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $('#book-update').dialog("close");
                            alert(textStatus + ' ' + jqXHR.status);
                        }
                    });
                },
                "Cancel": function () {
                    $('#book-update').dialog("close");

                }
            }

        });
    }


    function show_book_details(book_Id) {
        console.log('Book Id : ' + book_Id);
        $.ajax({
            url: '/api/' + book_Id,
            success: function (data) {
                $('#id').html(data[0].id);
                $('#name').html(data[0].name);
                $('#author').html(data[0].author);
                $('#publisher').html(data[0].publisher);
                $('#year').html(data[0].year);
                $('#page').html(data[0].page);
                $('#price').html(data[0].price);
                $('#store').html(data[0].store.name);
                $('#book-details').dialog("open");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 404) {
                    alert('Book id ' + bookId + ' not found! status = ' + textStatus + ' error = ' + errorThrown);
                } else if (jqXHR.status === 500) {
                    alert('Book id ' + bookId + ' not found! status = ' + textStatus + ' error = ' + errorThrown);
                }
            }

        });
    }

    function show_book_update(book_Id) {
        console.log("Book Id book Update : " + book_Id)
        $.ajax({
            url: '/api/' + book_Id,
            success: function (data) {
                $('#upd_name').val(data[0].name);
                $('#upd_author').val(data[0].author);
                $('#upd_publisher').val(data[0].publisher);
                $('#upd_year').val(data[0].year);
                $('#upd_page').val(data[0].page);
                $('#upd_price').val(data[0].price);
                $.ajax({
                    url: '/api/store/name',
                    success: function (data) {
                        console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            $('#upd_store').append("<option value=" + data[i].id +
                                ">" + data[i].name + "</option>");
                        }
                    }
                });
                $('#id_book').val(data[0].id);
                $('#book-update').dialog("open");
            }
        });
    }

    function show_book_delete(book_id) {


        var table = $('#book-list').DataTable();
        if (confirm("Do you sure to delete it")) {
            $(document).ajaxSend(function (e, xhr, options) {
                xhr.setRequestHeader(header, token);
            });
            $.ajax({
                url: 'api/delete/' + book_id,
                method: 'DELETE',
                success: function (data) {
                    table.ajax.reload(function (json) {
                        $('#myInput').val(json.lastInput);
                    });
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus + ' ' + jqXHR.status);
                    $('#book-update').dialog("close");
                }
            });
        }
    }

    function setup_datatable() {
        $('#book-list').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "api/"
            }
        });
    }

    $(document).ready(function () {
        console.log("Page load");
        setup_dialog();
        setup_datatable();
        $('#flexible-list').on('change', function () {
            $('#book-list').DataTable().ajax.reload();
        });

    })
</script>
</html>
