package com.bookstore.repository;

import com.bookstore.domain.Books;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookRepoTest {
    @Autowired BookRepo bookRepo;

    @Before
    public void setup(){
        System.out.println("Book repository hazirliq isleri");
    }

    @Test
    public void testBookList(){
        List<Books>booksList=bookRepo.getBooksList(10,0);
        assertTrue(booksList.size()>0);
        booksList.forEach(books -> {
            assertTrue(books.getId()>0,"Sert dogrudur");
            assertTrue(books.getName()!=null && !books.getName().isEmpty(),"sert dogrudur");
        });
    }
    @After
    public void cleanup(){
        System.out.println("Book repository temizlik isleri");
    }
}
