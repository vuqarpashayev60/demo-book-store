package com.bookstore.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommonRepoTest {
    @Autowired
    CommonRepo commonRepo;

    @Before
    public void setup() {
        System.out.println("common repository hazirliq isleri");
    }

    @Test
    public void testcheckUserName() {
        Boolean answer = commonRepo.checkUserName("Arif");
        assertTrue(answer);
    }

    @After
    public void cleanup() {
        System.out.println("common repository temizlik isleri");
    }
}
